const YAML = require('yaml');
const getPixels = require('get-pixels');
const fs = require('fs');
const { execSync } = require('child_process');

const config = YAML.parse(fs.readFileSync('config.yml', 'utf8'));
const frame = +fs.readFileSync('frame.txt', 'utf8') || 0;

// read env variables
const env = process.env;

const timeIntervalMillis = 1000 / config.fps;
const uv = {
    u: ((env.COL - 1) / (config.resolution.x - 1)),
    v: ((env.ROW - 1) / (config.resolution.y - 1))
}

// delete frame if it exists
if (fs.existsSync('frame.png')) {
    fs.unlinkSync('frame.png');
}

// calculate the timestamp

const timestampMillis = timeIntervalMillis * frame;
const timestamp = `${Math.floor(timestampMillis / 1000)}.${timestampMillis % 1000}`;

// run ffmpeg to get the frame
const ffmpegCommand = `ffmpeg -i ${config.videoFile} -ss ${timestamp} -frames 1 -f image2 frame.png`;

execSync(ffmpegCommand);

console.log(`Time: ${timestamp}`);

// read the frame

getPixels('frame.png', function(err, pixels) {
    if (err) {
        console.log('Bad image path');
        process.exit(2);
    }
    
    // get the dimensions of the frame
    const frameWidth = pixels.shape[0];
    const frameHeight = pixels.shape[1];

    console.log(`Frame: ${frameWidth}, ${frameHeight}`);
    console.log(`Resolution: ${config.resolution.x}, ${config.resolution.y}`);
    console.log(`UV: ${uv.u}, ${uv.v}`);
    // calculate the pixel position
    let pixelX = Math.round(frameWidth * uv.u);
    if(pixelX < 0) {
        pixelX = 0;
    }
    if(pixelX >= frameWidth) {
        pixelX = frameWidth - 1;
    }
    let pixelY = Math.round(frameHeight * uv.v);
    if(pixelY < 0) {
        pixelY = 0;
    }
    if(pixelY >= frameHeight) {
        pixelY = frameHeight - 1;
    }

    console.log(`Pixel: ${pixelX}, ${pixelY}`);

    // get the pixel
    const pixel = pixels.get(pixelX, pixelY, 0);
    console.log(pixel);

    if(pixel > 128) {
        console.log('White');
        process.exit(0);
    } else {
        console.log('Black');
        process.exit(1);
    }

})


