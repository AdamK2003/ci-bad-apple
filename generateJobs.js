const YAML = require('yaml');
const fs = require('fs');


let config = YAML.parse(fs.readFileSync('config.yml', 'utf8'));

let gitlabCI = {
    image: config.jobImage,
    stages: [],
};

let jobs = {};

jobTemplate = Object(config.jobTemplate);
let jobScript = jobTemplate.script;


for (let x=1; x <= config.resolution.x; x++) {
    let stageName = x.toString();
    gitlabCI.stages.push(stageName);
    for (let y=1; y <= config.resolution.y; y++) {
        let jobName = (x + '-' + y).toString();
        jobs[jobName] = {};
    }
}

for (let jobName in jobs) {
    let job = jobs[jobName];
    job = Object.assign(job, jobTemplate);
    job.stage = jobName.split('-')[0];
    job.variables = {
        COL: jobName.split('-')[0],
        ROW: jobName.split('-')[1],
    };
    if(config.scriptAsJS) {
        let x = +job.variables.COL;
        let y = +job.variables.ROW;
        if(typeof jobScript === 'string') {

            job.script = eval(jobScript).toString();
            //console.log(x, y, job.script);
        }
        if (typeof jobScript === 'array') {
            job.script = jobScript.map(script => eval(script).toString());
        }
    }
    
}

gitlabCI = Object.assign(gitlabCI, jobs);

fs.writeFileSync(config.jobFile, YAML.stringify(gitlabCI));

console.log('Generated jobs file:', config.jobFile);
