const YAML = require('yaml');
const fs = require('fs');


let config = YAML.parse(fs.readFileSync('config.yml', 'utf8'));

let githubCI = {
    name: 'CI',
    on: {
        push: {
            branches: ['main']
        },
            workflow_dispatch: {}
    },
    jobs: {}
};


let jobs = {};

jobTemplate = Object(config.githubJobTemplate);
let jobScript = jobTemplate.run;


for (let x=1; x <= config.resolution.x; x++) {
    let jobName = x.toString();
    jobs[jobName] = {};
}

for (let jobName in jobs) {
    let job = jobs[jobName];
    job = Object.assign(job, jobTemplate);
    job.stage = jobName.split('-')[0];
    job.strategy = {
        matrix: {
            x: [jobName],
            y: []
        }
    }
    
    job.strategy.matrix.y = Array.from({length: config.resolution.y}, (v, i) => (i+1).toString());

    
}

githubCI.jobs = jobs

fs.writeFileSync(config.githubJobFile, YAML.stringify(githubCI));

console.log('Generated jobs file:', config.githubJobFile);
